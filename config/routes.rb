Rails.application.routes.draw do
  get '/register'  => 'user#new'
  post '/register' => 'user#create'
  get '/u/:username' => 'user#show'
  post '/u/:username/delete' => 'user#delete'
  post '/u/:username/edit' => 'user#update'

  get '/login' => 'login#new'
  post '/login' => 'login#create'
  post '/logout' => 'login#delete'

  get '/' => 'category#index'
  get '/?page=:page' => 'category#index'
  get '/create' => 'category#new'
  post '/create' => 'category#create'
  get '/c/:id' => 'category#show'
  get '/c/:id/?page=:page' => 'category#show'

  get '/u/:username/subscriptions' => 'subscribe#index'
  get '/u/:username/subscriptions/?page=:page' => 'subscribe#index'
  post '/c/:id/subscribe' => 'subscribe#create'
  post '/c/:id/unsubscribe' => 'subscribe#delete'

  get '/c/:id/post/create' => 'post#new'
  post 'c/:id/post/create/' => 'post#create'
  get '/c/:id/p/:p_id' => 'post#show'
  get '/c/:id/p/:p_id/?page=:page' => 'post#show'

  get '/c/:id/p/:p_id/create' => 'comment#new'
  post '/c/:id/p/:p_id/create' => 'comment#create'
  get '/c/:id/p/:p_id/r/:r_id' => 'comment#show'

  match '*path', to: 'application#not_found', via: :all
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
