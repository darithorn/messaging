# messaging
A message board written using Rails

![sample_01](https://github.com/darithorn/messaging/blob/master/screens/sample_01.png)

![sample_02](https://github.com/darithorn/messaging/blob/master/screens/sample_02.png)

![sample_03](https://github.com/darithorn/messaging/blob/master/screens/sample_03.png)
