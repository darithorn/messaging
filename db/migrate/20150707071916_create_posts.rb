class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|

      t.timestamps null: false
      t.integer :category_id
      t.integer :user_id
      t.string :title
      t.text :content
    end
  end
end
