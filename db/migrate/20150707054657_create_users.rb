class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      t.timestamps null: false
      t.string :username
      t.text :password_digest # Should I separate sensitive information?
    end
  end
end
