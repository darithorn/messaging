class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|

      t.timestamps null: false
      t.integer :user_id, null: false
      t.integer :category_id, null: false
    end
  end
end
