class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|

      t.timestamps null: false
      t.integer :parent_id
      t.integer :user_id
      t.boolean :is_top # Top Level Comment
      t.text :content
    end
  end
end
