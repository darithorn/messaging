class CategoryController < ApplicationController
  def index
    if @logged_in
      @posts = feed
    else
    end
    @posts ||= []
    @next = false
    @prev = page > 0
  end

  def show
    return all if params[:id] == 'all'
    @posts = @category.posts page
    @next = @posts.length == Post.batch_size # Used to display next page button
    @prev = page > 0 # Used to display prev page button
  end

  def new
  end

  def create
    permitted = category_params
    name_taken = !Category.get(permitted[:name]).nil?
    invalid_name = invalid_name?(permitted)
    error = invalid_name || name_taken || !@logged_in
    if !error
      @category = Category.new(permitted)
      return redirect_to @category.url if @category.save
      flash[:save_failed] = 'There was an error saving the category.'
    end
    flash[:not_logged_in] = 'You are not logged in!' unless @logged_in
    flash[:name_taken] = 'That category name is already taken.' if name_taken
    flash[:invalid_name] = 'That name is invalid!' if invalid_name
  end

  def all
    @posts = []
    @next = false
    @prev = false
  end

  protected

  def feed(amount = 25)
    feed = []
    @user.category_subscriptions do |categories|
      categories.each do |category|
        unless category.nil?
          posts = Post.find_sorted_category(category.id)
          posts.each_index do |i|
            post = posts[i]
            if i >= feed.length
              feed = feed.push post
            elsif post.older? feed[i]
              feed = feed.insert(i, post)
            end
          end
        end
        feed = feed[0..25] if feed.length > amount
      end
    end
    feed
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end

  def invalid_name?(permitted)
    name = permitted[:name]
    name.strip.length == 0 && !name.match(/\s+/).nil?
  end
end
