require 'bcrypt'

class UserController < ApplicationController
  def show
    user = User.find_by_username(params[:username]) || not_found
    @public = {}
    @public[:username] = user.username
    @public[:url] = user.url
    @public[:same_user] = @logged_in && @user.id == user.id
  end

  def new
  end

  def create
    permitted = user_params
    username_taken = !User.find_by_username(user_params[:username]).nil?
    invalid_name = invalid_name? permitted[:username]
    invalid_password = invalid_password? permitted[:password]
    error = invalid_name || username_taken || invalid_password
    unless error
      user = User.new(permitted)
      if user.save
        session[:current_user_id] = user.id
      else
        flash[:save_failed] = 'Failed to save User!'
      end
    end
    flash[:username_taken] = 'That username is taken.' if username_taken
    flash[:invalid_username] = 'That username is invalid!' if invalid_name
    flash[:invalid_password] = 'That password is invalid!' if invalid_password
    redirect_to request.referer
  end

  def update
    permitted = update_params
    update_hash = {}
    error = permitted[:username] != @user.username && flash[:invalid_username] = 'You cannot edit another user\'s information!'
    error ||= !User.authenticate(permitted[:username], permitted[:password]) && flash[:invalid_credentials] = 'Your credentials were invalid.'
    unless error
      change_password = !invalid_password?(permitted[:new_password])
      if change_password
        update_hash[:password] = permitted[:new_password]
        update_hash[:password_confirmation] = permitted[:new_password_confirmation]
      end
      if @user.update(update_hash)
        flash[:updated_info] = 'Your information was changed successfully'
      else
        flash[:updated_failed] = 'Your information failed to update.'
      end
    end
    redirect_to "/u/#{params[:username]}"
  end

  def delete
    unless @logged_in
      flash[:not_logged_in] = 'You need to be logged in!'
      return
    end
    permitted = delete_params
    if User.authenticate @user.username, permitted[:password]
      @user.delete
      @logged_in = false
      @user = nil
      session.delete :current_user_id
    else
      flash[:invalid_credentials] = 'Your credentials were incorrect.'
    end
    redirect_to request.referer
  end

  private

  def user_params
    params.require(:user).permit(:username, :password, :password_confirmation)
  end

  def update_params
    params.require(:user).permit(:username, :new_password, :new_password_confirmation, :password)
  end

  def delete_params
    params.require(:user).permit(:password)
  end

  def invalid_name?(username)
    username.blank? ||
      username.strip.length < 3 ||
      username.strip.length > 15
  end

  def invalid_password?(password)
    password.blank? ||
      password.strip.length < 8
  end
end
