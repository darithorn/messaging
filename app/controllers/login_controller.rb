class LoginController < ApplicationController
  # Login (get)
  def new
  end

  # Login (post)
  def create
    permitted = login_params
    user = User.authenticate(permitted[:username], permitted[:password])
    if !user.nil?
      session[:current_user_id] = user.id
      #Session.reset_session(session)
    else
      flash[:invalid_login] = 'The username and password is not correct!'
    end
    redirect_to request.referer
  end

  # Logout
  def delete
    @logged_in = false
    @user = nil
    session.delete :current_user_id
    redirect_to request.referer
  end

  private

  def login_params
    params.require(:login).permit(:username, :password)
  end
end
