class PostController < ApplicationController
  def show
    replies
    @more = @comments.length == Post.batch_size # Used to display `Show more`
  end

  def replies
    @post = Post.get(params[:p_id], @category.id) || not_found
    @comments = @post.comments page
  end

  def new
  end

  def create
    permitted = post_params
    invalid_title = invalid_title? permitted
    invalid_content = invalid_content? permitted
    error = invalid_title || invalid_content || !@logged_in || @category.nil?
    unless error
      permitted[:category_id] = @category.id
      permitted[:user_id] = session[:current_user_id]
      @post = Post.new(permitted)
      return redirect_to @post.url if @post.save
      flash[:save_failed] = 'There was an error saving the post.'
    end
    flash[:not_logged_in] = 'You need to be logged in!' unless @logged_in
    flash[:invalid_title] = 'That title is invalid.' if invalid_title
    flash[:invalid_content] = 'The content is invalid!' if invalid_content
  end

  private

  def post_params
    params.require(:post).permit(:title, :content)
  end

  def invalid_title?(permitted)
    title = permitted[:title]
    title.strip.length == 0
  end

  def invalid_content?(permitted)
    permitted[:content].strip.length == 0
  end
end
