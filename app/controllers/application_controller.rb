class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead
  protect_from_forgery with: :exception
  rescue_from ActionController::RoutingError, with: :page_404
  before_filter :titlebar

  def titlebar
    @logged_in = logged_in?
    if @logged_in
      @user = current_user
    end
    @show_actions = false
    if !params.key? :id
      @category_url = '/'
      @display_url = '/c/home'
      @home = true
    elsif params[:id] == 'all'
      @category_url = '/c/all'
      @display_url = @category_url
    else
      @show_actions = true
      @category = Category.find_by_name(params[:id]) || not_found
      @subscribed = @logged_in && !Subscription.find_by_ids(@user.id, @category.id).nil?
      @category_url = @category.url
      @display_url = @category_url
    end
    @home ||= false
  end

  def not_found
    raise ActionController::RoutingError.new('Not found!')
  end

  protected

  def logged_in?
    session.key?(:current_user_id) && !User.find_by(id: session[:current_user_id]).nil?
  end

  def current_user
    return User.find_by(id: session[:current_user_id]) if session.key? :current_user_id
    User.new
  end

  def page
    if params.key?(:page) then params[:page].to_i else 0 end
  end

  private

  def page_404
    render file: "#{Rails.root}/public/404", layout: nil, status: 404
  end
end
