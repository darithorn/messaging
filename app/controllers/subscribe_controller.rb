class SubscribeController < ApplicationController
  def index
    if @logged_in
      if @user.same? User.find_by_username(params[:username])
        @subscriptions = Subscription.find_by_user_id(@user.id, page)
        @categories = []
        @subscriptions.each do |sub|
          not_duplicate = true
          temp = Category.find_by_id(sub.category_id)
          @categories.each do |cat|
            if temp.name == cat.name then not_duplicate = false end
          end
          @categories.push temp if not_duplicate
        end
        @categories.compact!
      else
        flash[:not_correct_user] = 'You cannot access this user\'s subscriptions' unless correct_user
      end
    else
      flash[:not_logged_in] = 'You need to be logged in to access your subscriptions.'
    end
    @subscriptions ||= []
    @categories ||= []
  end

  def create
    invalid_category = @category.nil?
    error = invalid_category || !@logged_in
    unless error
      not_subscribed = Subscription.find_by_ids(@user.id, @category.id).nil?
      if not_subscribed
        subscription = Subscription.new(user_id: @user.id,
                                        category_id: @category.id)
        return redirect_to request.referer if subscription.save
        flash[:save_failed] = 'Unable to save Subscription model!'
      else
        flash[:already_subscribed] = 'You\'re already subscribed.'
      end
    end
    flash[:not_logged_in] = 'You need to be logged in to subscribe.' unless @logged_in
    flash[:not_found] = "#{params[:id]} was not found!" if @category.nil?
    redirect_to request.referer
  end

  def delete
    category_valid = !@category.nil?
    if @logged_in && category_valid
      return redirect_to request.referer if Subscription.delete_with_ids(@user.id, @category.id)
      flash[:unable_to_delete] = 'There was an error deleting the subscription.'
    end
    flash[:invalid_category] = 'You cannot unsubscribe from that category.' unless category_valid
    flash[:not_logged_in] = 'You need to be logged in!' unless @logged_in
  end
end
