class CommentController < ApplicationController
  def show
    @comment = Comment.find_by_id(params[:r_id]) || not_found
  end

  def new
  end

  def create
    permitted = comment_params
    logged_in = Session.logged_in? session
    valid_id = valid_id? permitted[:parent_id], permitted[:is_top]
    valid_content = valid_content? permitted[:content]
    valid = logged_in && valid_id && valid_content
    if valid
      permitted[:user_id] = session[:current_user_id]
      @comment = Comment.new(permitted)
      return redirect_to @comment.url if @comment.save
      flash[:saving_error] = 'There was an error saving the comment.'
    end
    flash[:not_logged_in] = 'You need to be logged in to comment.' unless logged_in
    flash[:invalid_id] = 'The specified id is invalid.' unless valid_id
    flash[:invalid_content] = 'The comment content is too short.' unless valid_content
    redirect_to :new
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :is_top, :parent_id)
  end

  def valid_id?(id, is_top)
    return !Post.find_by_id(id).nil? if is_top
    !Comment.find_by_id(id).nil?
  end

  def valid_content?(content)
    content.strip.length > 0
  end
end
