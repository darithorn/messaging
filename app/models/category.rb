class Category < ActiveRecord::Base
  class << self
    def batch_size
      25
    end

    def find_by_id(id)
      Category.find_by(id: id)
    end

    def get(name)
      Category.find_by(name: name)
    end
  end

  def url
    "/c/#{name}"
  end

  def posts(page)
    Post.where("category_id == \"#{id}\"").offset(page * Category.batch_size).limit(Category.batch_size)
  end
end
