class Comment < ActiveRecord::Base
  class << self
    def batch_size
      25
    end
  end

  def top
    if is_top
      Post.find_by(id: parent_id)
    else
      Comment.find_by(id: parent_id).top
    end
  end

  def url
    top_comment = top
    if !top_comment.nil?
      top_comment.url.concat("/r/#{id}")
    else
      fail ArgumentError, "#{parent_id} does not correlate with a valid parent!"
    end
  end

  def parent
    if is_top
      Post.find_by(id: parent_id)
    else
      Comment.find_by(id: parent_id)
    end
  end

  def comments(page)
    Comment.where("parent_id == #{id}").offset(page * Comment.batch_size).limit(Comment.batch_size)
  end
end
