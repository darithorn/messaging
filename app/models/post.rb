class Post < ActiveRecord::Base
  class << self
    def batch_size
      25
    end

    def find_sorted_category(id, page = 0)
      Post.where("category_id == #{id}").offset(page * batch_size).order(created_at: :asc).limit(batch_size)
    end

    def find_by_id(id)
      Post.find_by(id: id)
    end

    def find_by_category_id(id)
      Post.find_by(category_id: id)
    end

    def get(id, category_id)
      Post.find_by(id: id, category_id: category_id)
    end
  end

  def url
    "/c/#{category.name}/p/#{id}"
  end

  def category
    Category.find_by_id(category_id)
  end

  def comments(page)
    Comment.where("parent_id == #{id}").offset(page * Post.batch_size).limit(Post.batch_size)
  end

  def older?(other)
    created_at > other.created_at
  end
end
