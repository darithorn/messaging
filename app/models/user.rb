require 'bcrypt'

class User < ActiveRecord::Base
  has_secure_password
  validates :password, length: { minimum: 8 }, allow_nil: true
  validates :username,
    presence: true,
    uniqueness: { case_sensitive: false }

  has_many :subscriptions, dependent: :destroy

  after_save :create_default_subs

  class << self
    def authenticate(username, password)
      user = find_by(username: username)
      if !user.nil?
        if BCrypt::Password.new(user.password_digest).is_password? password
          return user
        end
      end
      nil
    end

    def find_by_username(name)
      User.find_by(username: name)
    end

    def get(username)
      User.find_by(username: username)
    end
  end

  def url
    "/u/#{username}"
  end

  def same?(other)
    return false if other.nil?
    other.id == id
  end

  def category_subscriptions
    Subscription.get_user_subs(id) do |subs|
      categories = Array.new(subs.length)
      subs.each_index do |i|
        sub = subs[i]
        categories[i] = Category.find_by_id(sub.category_id)
      end
      yield(categories)
    end
  end

  def create_default_subs
    Subscription.transaction do
      Subscription.default_subs.each do |cat|
        Subscription.create(user_id: id, category_id: cat.id) unless cat.nil?
      end
    end
  end
end
