class Subscription < ActiveRecord::Base
  belongs_to :user

  class << self
    def batch_size
      50
    end

    def default_subs
      [Category.find_by_name('ComputerScience')]
    end

    def get_user_subs(id)
      Subscription.where("user_id == #{id}").find_in_batches do |subs|
        yield(subs)
      end
    end

    def find_by_user_id(id, page = 0)
      Subscription.where("user_id == #{id}").offset(page * batch_size).limit(batch_size)
    end

    def find_by_ids(user_id, category_id)
      Subscription.find_by(user_id: user_id, category_id: category_id)
    end

    def delete_with_ids(user_id, category_id)
      Subscription.where("user_id == #{user_id} AND category_id == #{category_id}").delete_all
    end
  end

  def user
    User.find_by_id(user_id)
  end

  def category
    Category.find_by_id(category_id)
  end
end
