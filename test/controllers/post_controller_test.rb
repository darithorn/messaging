require 'test_helper'

class PostControllerTest < ActionController::TestCase
  def self.user_id 
    User.find_by(username: 'darithorn').id
  end

  test 'post_show' do
    category = Category.get('Test')
    assert category, 'category was nil!'
    posts = category.posts 0
    assert posts, 'posts was nil!'
    get :show, { id: 'Test', p_id: posts[0].id }
    assert_response :success
    assert assigns(:comments), ':comments is nil!'
    assert_not assigns(:more).nil?, ':more is nil!'
  end

  test 'post_show_page' do
    category = Category.get('Test')
    assert category
    posts = category.posts 0
    assert posts
    get :show, { id: 'Test', p_id: posts[0].id, pg: 3 }
    assert_response :success
  end

  test 'post_create' do
    post :create, 
         { id: 'Test', post: { title: 'Test Post!', content: 'This is a test post' } },
         { current_user_id: PostControllerTest.user_id }
    assert assigns(:post)
    assert_redirected_to assigns(:post).url
  end
end
