require 'test_helper'

class CategoryControllerTest < ActionController::TestCase
  def self.user_id 
    User.find_by(username: 'darithorn').id
  end

  test 'category_show' do
    get :show, { id: 'Test' }
    assert_response :success
  end

  test 'category_show_page' do
    page = 1
    get :show, { id: 'Test', pg: page }
    assert_response :success
    assert assigns(:posts), ':posts does not exist!'
    assert assigns(:posts).length == Category.batch_size, 'did not grab enough posts!'
  end

  test 'category_index' do
    get :index
    assert_response :success
  end

  test 'category_all' do
    get :show, { id: 'all' }
    assert_response :success
  end

  test 'category_create' do
    post :create,
         { category: { name: 'MyTestCategory' } },
         { current_user_id: CategoryControllerTest.user_id }
    assert assigns(:category), '@category wasn\'t assigned!'
    category = Category.find_by(name: 'MyTestCategory')
    assert category, 'category wasn\'t created'
    assert_redirected_to category.url, 'Didn\'t get redirected to category url!'
    assert_not flash.key?(:invalid_name), 'name was invalid! Should be valid!' # Then tests if the name is valid (it should)
  end
end
