require 'test_helper'

class UserControllerTest < ActionController::TestCase
  test 'user_show' do
    get :show, { username: 'darithorn' }
    assert_response :success
  end

  test 'user_create' do
    @request.headers['HTTP_REFERER'] = '/u/test_user'
    post :create,
          user: { username: 'test_user', password: 'test_password', password_confirmation: 'test_password' }
    assert_redirected_to '/u/test_user'
    assert session.key? :current_user_id
  end

  test 'user_register_bad' do
    @request.headers['HTTP_REFERER'] = '/u/test_user'
    post :create, user: { username: 'test_user', password: 'test_password', password_confirmation: 'wrong_confirmation'}
    assert_redirected_to '/u/test_user'
  end

  test 'user_delete' do
    @request.headers['HTTP_REFERER'] = '/'
    post :create,
         user: { username: 'test_user', password: 'test_password', password_confirmation: 'test_password'}
    assert_redirected_to '/'
    assert session.key? :current_user_id
    post :delete, { username: 'test_user', user: { password: 'test_password' } }
    assert_not session.key?(:current_user_id), 'current_user_id was not removed.'
    assert_not User.find_by_username('test_user')
  end
end
