require 'test_helper'

class LoginControllerTest < ActionController::TestCase
  test 'login_create' do
    @request.headers['HTTP_REFERER'] = '/c/Test'
    post :create, login: { username: 'darithorn', password: 'test_password' }
    assert_redirected_to '/c/Test'
    assert session.key? :current_user_id
  end

  test 'login_create_fail' do
    @request.headers['HTTP_REFERER'] = '/c/Test'
    post :create, login: { username: 'darithorn', password: 'wrong_password' }
    assert_redirected_to '/c/Test'
    assert flash[:invalid_login]
  end

  test 'logout' do
    @request.headers['HTTP_REFERER'] = '/c/Test'
    post :delete, nil, { current_user_id: 0 }
    assert_not session.key? :current_user_id
    assert_redirected_to '/c/Test'
  end
end
