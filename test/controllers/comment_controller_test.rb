require 'test_helper'

class CommentControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  test 'comment_create' do
    test_post = Post.find_by(title: 'Post 1')
    post :create,
         { id: 'Test', p_id: test_post.id, comment: { content: 'Test Comment!', is_top: true, parent_id: test_post.id } },
         { current_user_id: PostControllerTest.user_id }
    assert assigns(:comment), 'comment was nil!'
    assert_redirected_to assigns(:comment).url
  end

  test 'comment_show' do
    # Create a comment
    test_post = Post.find_by(title: 'Post 1')
    assert_not test_post.nil?, 'test_post was nil!'
    post :create,
         { id: 'Test', p_id: test_post.id, comment: { content: 'Test Comment!', is_top: true, parent_id: test_post.id } },
         { current_user_id: PostControllerTest.user_id }
    comment = test_post.comments(0)[0]
    assert_not comment.nil?, 'comment was nil!'
    # Try to retrieve it
    get :show, { id: 'Test', p_id: test_post.id, r_id: comment.id }
    assert_response :success
  end
end
