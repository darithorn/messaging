require 'test_helper'

class SubscribeControllerTest < ActionController::TestCase
  def self.subscribe_to(name)
    user = User.find_by_username('darithorn')
    return false if user.nil?
    category = Category.find_by_name(name)
    return false if category.nil?
    sub = Subscription.new({ user_id: user.id, category_id: category.id })
    sub.save
  end

  # Subscriptions
  test 'subscribe_index' do
    assert SubscribeControllerTest.subscribe_to('Test')
    assert SubscribeControllerTest.subscribe_to('Derp')
    get :index, { username: 'darithorn', page: 0 }, { current_user_id: 0 }
    assert_response :success
    assert_not flash.key?(:not_correct_user), 'Not correct user.'
    assert_not flash.key?(:not_logged_in), 'Not logged in.'
    assert assigns(:subscriptions), 'subscriptions not assigned'
    assert assigns(:subscriptions).length == 2, 'length is not 2'
  end

  # Subscribe
  test 'subscribe_create' do
    @request.headers['HTTP_REFERER'] = '/c/Test'
    post :create, { id: 'Test' }, { current_user_id: 0 }
    assert_not flash.key?(:already_subscribed), 'Already subscribed.'
    assert_redirected_to '/c/Test'
  end

  # Unsubscribe
  test 'subscribe_delete' do
    assert SubscribeControllerTest.subscribe_to('Test')
    @request.headers['HTTP_REFERER'] = '/c/Test'
    post :delete, { id: 'Test' }, { current_user_id: 0 }
    assert_not flash.key?(:invalid_category), 'Invalid category!'
    assert_not flash.key?(:not_logged_in), 'Not logged in!'
  end
end
