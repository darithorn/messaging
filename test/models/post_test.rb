require 'test_helper'

class PostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test 'post_get' do
    category = Category.find_by(name: 'Test')
    assert category, 'category is nil!'
    post = Post.find_by(category_id: category.id)
    assert post, 'post is nil!'
  end
end
