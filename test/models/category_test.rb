require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test 'category_get' do
    category = Category.find_by(name: 'Test')
    assert category
  end

  test 'category_posts' do
    category = Category.find_by(name: 'Test')
    assert category
    posts = category.posts(0)
    assert posts
    assert posts.length == Category.batch_size
  end
end
