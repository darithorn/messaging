require 'test_helper'
require 'bcrypt'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  @@default_username = 'darithorn'
  @@default_password = 'test_password'

  def self.create_default_user
    user = User.new(username: @@default_username,
                    password: @@default_password,
                    password_confirmation: @@default_password)
    user.save
  end

  UserTest.create_default_user

  def self.default_user
    User.find_by_username(@@default_username)
  end

  test 'user_creation' do
    assert UserTest.default_user
  end

  test 'user_password_hash' do
    user = UserTest.default_user
    assert user
    assert user.password_digest
    assert BCrypt::Password.new(user.password_digest).is_password? 'test_password'
  end

  test 'user_authentication' do
    assert UserTest.default_user, 'Failed to create user.'
    assert User.authenticate('darithorn', 'test_password')
  end
end
